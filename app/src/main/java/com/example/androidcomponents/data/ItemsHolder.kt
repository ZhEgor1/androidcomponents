package com.example.androidcomponents.data

object ItemsHolder {

    val items by lazy {
        (0 until 20).map {
            Item(it, "name - $it", "description - $it")
        }
    }

    fun getById(id: Int): Item {
        if (id == -1) return Item(-1, "Empty name", "Empty description")
        for (item in items) {
            if (item.id == id)
                return item
        }
        return Item(-1, "Empty name", "Empty description")
    }

}
