package com.example.androidcomponents.data

data class Item(var id: Int, var name: String?, var description: String?)
