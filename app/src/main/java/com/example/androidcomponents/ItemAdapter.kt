package com.example.androidcomponents

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.androidcomponents.data.Item
import com.example.androidcomponents.databinding.ItemRowBinding

class ItemAdapter(private val items: List<Item>, private val itemClickListener: ItemClickListener)
    : ListAdapter<Item, ItemAdapter.ItemHolder>(ItemDiffCallback()) {

    class ItemHolder(private val binding: ItemRowBinding) : RecyclerView.ViewHolder (binding.root) {
        fun bind(item: Item, onClick: (Item) -> Unit) {
            binding.textView.text = item.name
            binding.root.setOnClickListener {
                onClick(item)
            }
        }
    }
    interface ItemClickListener {
        fun onItemClick(itemID: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val binding = ItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bind(items[position]) {
            itemClickListener.onItemClick(position)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class ItemDiffCallback : DiffUtil.ItemCallback<Item>() {
    override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
        return oldItem.id == oldItem.id
    }

    override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
        return areItemsTheSame(oldItem, newItem)
    }

}
