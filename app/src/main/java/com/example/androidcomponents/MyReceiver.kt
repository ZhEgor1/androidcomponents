package com.example.androidcomponents

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.androidcomponents.const.AppPreferences
import com.example.androidcomponents.const.IntentKey
import com.example.androidcomponents.screens.MainActivity

class MyReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val sPref = context.getSharedPreferences(AppPreferences.IT , Service.MODE_PRIVATE)
        val itemID = sPref.getInt(AppPreferences.ID, -1)
        val intentItem = Intent(context, MainActivity::class.java)
        intentItem.putExtra(IntentKey.ITEM_ID, itemID)
            .flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intentItem)
    }

}
