package com.example.androidcomponents

import android.app.*
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import com.example.androidcomponents.const.Action

class MyService : Service() {
    private lateinit var myReceiver: BroadcastReceiver
    override fun onBind(intent: Intent): IBinder {

        TODO("Return the communication channel to the service.")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        createNotificationChannel()
        val intentNotif = Intent(Action.NOTIFICATION_CLICK)
        myReceiver = MyReceiver()
        val pendingIntent = PendingIntent.getBroadcast(this, 0, intentNotif, 0)
        val intentFilter = IntentFilter(Action.NOTIFICATION_CLICK)
        registerReceiver(myReceiver, intentFilter)
        val notif = Notification.Builder(this, "ChannelId")
            .setContentTitle("Android Components")
            .setContentText("Last item")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pendingIntent)
            .build()
        startForeground(1, notif)
        return START_STICKY
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notifChannel = NotificationChannel("ChannelId", "Foreground notification", NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(notifChannel)
        }
    }

    override fun onDestroy() {
        stopForeground(true)
        stopSelf()
        unregisterReceiver(myReceiver)
        super.onDestroy()
    }
}
