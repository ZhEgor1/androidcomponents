package com.example.androidcomponents.const

object FragmentTag {
    const val ITEM_INFO = "ItemInfo_fragment"
    const val LIST_ITEM = "ListItem_fragment"

}
