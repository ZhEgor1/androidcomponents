package com.example.androidcomponents.screens.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidcomponents.ItemAdapter
import com.example.androidcomponents.R
import com.example.androidcomponents.const.AppPreferences
import com.example.androidcomponents.const.FragmentTag
import com.example.androidcomponents.data.ItemsHolder
import com.example.androidcomponents.databinding.FragmentListItemBinding


class ListItemFragment : Fragment(), ItemAdapter.ItemClickListener {

    private lateinit var binding: FragmentListItemBinding
    private lateinit var sPref: SharedPreferences


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListItemBinding.inflate(LayoutInflater.from(context), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }
    private fun init() {
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.adapter = ItemAdapter(ItemsHolder.items, this)
        sPref = activity?.getSharedPreferences(AppPreferences.IT, Context.MODE_PRIVATE)!!
    }

    override fun onItemClick(itemID: Int) {

        val fragment = ItemInfoFragment.newInstance(itemID)
        val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
        activity?.supportFragmentManager?.findFragmentByTag(FragmentTag.LIST_ITEM)?.let {
            fragmentTransaction?.hide(
                it
            )
        }
        fragmentTransaction?.add(R.id.fragment, fragment)
        fragmentTransaction?.addToBackStack(null)
        fragmentTransaction?.commit()
        val editor = sPref.edit()
        editor.putInt(AppPreferences.ID, itemID).apply()

    }
}
