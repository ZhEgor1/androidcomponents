package com.example.androidcomponents.screens.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.androidcomponents.data.Item
import com.example.androidcomponents.data.ItemsHolder
import com.example.androidcomponents.databinding.FragmentItemInfoBinding


private const val ARG_ITEM_ID = "itemID"

class ItemInfoFragment : Fragment() {

    private var itemID = -1
    private lateinit var binding: FragmentItemInfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            itemID = it.getInt(ARG_ITEM_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentItemInfoBinding.inflate(LayoutInflater.from(context), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       init()
    }
    private fun init() {
        val item = ItemsHolder.getById(itemID)
        binding.tvId.text = item.id.toString()
        binding.tvName.text = item.name
        binding.tvDescription.text = item.description
    }
    companion object {
        fun newInstance(itemID: Int) =
            ItemInfoFragment().apply {
                arguments = Bundle().apply {
                   putInt(ARG_ITEM_ID, itemID)
                }
            }
    }
}
