package com.example.androidcomponents.screens

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidcomponents.*
import com.example.androidcomponents.const.FragmentTag
import com.example.androidcomponents.const.IntentKey
import com.example.androidcomponents.screens.fragments.ItemInfoFragment
import com.example.androidcomponents.screens.fragments.ListItemFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init() {

        val fragment = ListItemFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment, fragment, FragmentTag.LIST_ITEM).commit()
        val intent = Intent(this, MyService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
        } else {
            startService(intent)
        }
        reactionToIntent()
    }
    private fun reactionToIntent() {
        val itemID = intent.getIntExtra(IntentKey.ITEM_ID, -1)
        if (itemID != -1) {
            val fragment = ItemInfoFragment.newInstance(itemID)
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.addToBackStack(FragmentTag.LIST_ITEM)
                .replace(R.id.fragment, fragment, FragmentTag.ITEM_INFO).commit()
        }
    }
}
